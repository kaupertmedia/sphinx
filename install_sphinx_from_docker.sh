#!/bin/env bash

# docker build . -t sphinx_builder
CONTAINER_ID=$(docker run -it -d sphinx_builder /bin/sh)
docker cp $CONTAINER_ID:/usr/local/bin/indexer .
docker cp $CONTAINER_ID:/usr/local/bin/search .
docker cp $CONTAINER_ID:/usr/local/bin/searchd .
docker cp $CONTAINER_ID:/usr/local/bin/spelldump .
sudo docker cp $CONTAINER_ID:/usr/lib/x86_64-linux-gnu/libmysqlclient.so.18.0.0 /usr/lib/x86_64-linux-gnu/libmysqlclient.so.18
