FROM gcc:5.4

COPY . .

RUN apt-get update -y
RUN apt-get install -y \
  build-essential \
  mysql-client \
  libmysqlclient-dev

RUN gcc --version

RUN ./configure --prefix /usr/local && make && make install clean && make clean

EXPOSE 3312
