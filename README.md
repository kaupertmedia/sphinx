# Sphinx

Fork of v0.9.8.1 of the Sphinx search engine, refined to work with modern C++ compilers.

Tested with:

* c++ 4.2.1 Apple LLVM version 5.1 (clang-503.0.40) Xcode 5.1.1
* c++ 4.2.1 Apple LLVM version 7.3.0 (clang-703.0.29)
* g++ 4.7.2 Debian GNU/Linux (Wheezy)
* g++ 4.8.4 Ubuntu 14.04 (Trusty)
* g++ 5.4.0 Ubuntu 16.04 (Xenial)

## Installation

```sh
git clone git@bitbucket.org:kaupertmedia/sphinx.git
cd sphinx
./configure && make && sudo make install
```

Run this to compile + then copy bins out of the docker image:

```sh
./install_sphinx_from_docker.sh
```
